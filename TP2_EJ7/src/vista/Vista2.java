package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import java.awt.Color;
import javax.swing.table.DefaultTableModel;

import controlador.ControladorE6;
import controlador.ControladorVista2;

import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import java.awt.Font;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.awt.event.ActionEvent;

public class Vista2 extends JFrame {

	private JPanel contentPane;
	private ControladorVista2 controlador;
	private JTable table_1;
	private JButton btnExplorar;
	private JTextField textExplorar;

	public Vista2(ControladorVista2 controlador) {
		this.setControlador(controlador);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 872, 337);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setForeground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JScrollPane scrollPane = new JScrollPane();
		
		btnExplorar = new JButton("Explorar"); //BOTON DE BUSCAR
		btnExplorar.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnExplorar.addActionListener(this.getControlador());
		
		textExplorar = new JTextField();
		textExplorar.setColumns(10);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(22)
					.addComponent(textExplorar, GroupLayout.PREFERRED_SIZE, 614, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
					.addComponent(btnExplorar, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
					.addGap(62))
				.addGroup(Alignment.LEADING, gl_contentPane.createSequentialGroup()
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 845, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnExplorar)
						.addComponent(textExplorar, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
					.addGap(30)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE)
					.addGap(12))
		);
		
		table_1 = new JTable();
		table_1.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Nombre", "Genero", "Email", "Fecha"
			}
		) {
			Class[] columnTypes = new Class[] {
				String.class, String.class, String.class, String.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		scrollPane.setViewportView(table_1);
		
	
		contentPane.setLayout(gl_contentPane);
	}

	public ControladorVista2 getControlador() {
		return controlador;
	}

	public void setControlador(ControladorVista2 controlador) {
		this.controlador = controlador;
	}

	public JTable getTable() {
		return table_1;
	}
	
	public void cargarFila(String nombre, String genero, String email, String fecha) {
		int numCols = table_1.getModel().getColumnCount();
		Object [] fila = new Object[numCols]; 
		 fila[0] = nombre;
		 fila[1] = genero;
		 fila[2] = email;
		 fila[3] = fecha;
		 ((DefaultTableModel) table_1.getModel()).addRow(fila);
	}
	public void borrarFilas() {
		for (int i = table_1.getModel().getRowCount() -1; i>=0;i--) {
			((DefaultTableModel) table_1.getModel()).removeRow(i);
		}
	}
	public JButton getBtnExplorar() {
		return btnExplorar;
	}

	public JTextField getTextExplorar() {
		return textExplorar;
	}
	
}
