package modelo;

import java.time.LocalDate;

public class Persona {

	private String nombre, mail,contrasena;
	private Character genero;
	private LocalDate fechaNacimiento;
//C
public Persona() {
		this("",'a',"",LocalDate.of(0, 0, 0),"");
	}
	public Persona(String nombre, Character genero, String mail, LocalDate fechaNacimiento, String contrasena) {
	this.setNombre(nombre);
	this.setGenero(genero);
	this.setMail(mail);
	this.setFechaNacimiento(fechaNacimiento);
	this.setContrasena(contrasena);
}

	//M
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Character getGenero() {
		return genero;
	}
	public void setGenero(Character c) {
		this.genero = c;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) { //sacar
		this.contrasena = contrasena;
	}
	public void imprimirInfo (Persona unaPersona) {
		System.out.println("Nombre: "+ this.getNombre());
		System.out.println("Fecha de nacimiento: "+ this.getFechaNacimiento());
		System.out.println("Genero: "+ this.getGenero());
		System.out.println("Email: "+this.getMail());
	}
}
