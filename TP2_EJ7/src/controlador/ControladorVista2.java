package controlador;

//import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
//import javax.swing.table.DefaultTableModel;

import modelo.Listado;
import modelo.Persona;
import vista.Vista2;

public class ControladorVista2 implements ActionListener {
	private Vista2 vista;
	private Listado listado;

	public ControladorVista2() {
		this.vista = new Vista2(this);
		this.listado = new Listado();
		this.vista.setVisible(true);
		llenarFilas(); 
	
	}
	public Vista2 getVista() {
		return vista;
	}
	public void setVista(Vista2 vista) {
		this.vista = vista;
	}
	public Listado getListado() {
		return listado;
	}
	public void setListado(Listado listado) {
		this.listado = listado;
	}
	public void llenarFilas() { //modificar datos de la fila
		ArrayList<Persona> personas = new ArrayList<Persona>();
		getListado().leerArchivo(personas);
		for (Persona unapersona : personas) {			
			String nombre = unapersona.getNombre();
			Character genero = unapersona.getGenero();
			String email = unapersona.getMail();
			LocalDate fecha = unapersona.getFechaNacimiento();
			this.getVista().cargarFila(nombre, genero.toString(), email, fecha.toString());
		}
		getListado().cargarArchivo(personas);
	}
	
	public void actionPerformed (ActionEvent e) {
		if(e.getSource().equals(getVista().getBtnExplorar())) {
			//Aca deberia limpiar la tabla 
			ArrayList<Persona> personas = new ArrayList<Persona>();
			getListado().leerArchivo(personas);
			String ingreso = getVista().getTextExplorar().getText();
			Pattern pat = Pattern.compile(ingreso);
			getVista().borrarFilas();
			for (Persona unapersona : personas) {		
				Matcher mat = pat.matcher(unapersona.getNombre());
				if(mat.find()) { //usar un metodo que reconosca las primeras letras del nombre
				String nombre = unapersona.getNombre();
				Character genero = unapersona.getGenero();
				String email = unapersona.getMail();
				LocalDate fecha = unapersona.getFechaNacimiento();
				this.getVista().cargarFila(nombre, genero.toString(), email, fecha.toString());
				}
			}
			getListado().cargarArchivo(personas);
		}
	}

}
