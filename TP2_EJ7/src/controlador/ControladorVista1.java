package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Scanner;

import javax.swing.JButton;

import modelo.Banco;
import modelo.Listado;
import modelo.Persona;
import vista.Vista1;
import vista.VistaPunto5;

public class ControladorVista1 implements ActionListener{
private Vista1 vista;
private Listado listado;

public ControladorVista1() {

	this.vista = new Vista1(this);
	this.listado = new Listado();
	this.vista.setVisible(true);
}
public void actionPerformed (ActionEvent e) { 
	if(e.getSource().equals(getVista().getBtnGuardar())) {
		ArrayList<Persona> clientes = new ArrayList<Persona>();
		//bajo archivo csv
		getListado().leerArchivo(clientes);
		//Guardo datos ingresados en pantalla
		String nombre = getVista().getTextNombre().getText();
		String apellido = getVista().getTextApellido().getText();
		String genero = getVista().getTextGenero().getText();
		String mail = getVista().getTextEmail().getText();
		String contrasena = getVista().getTextContraseña().getText();
		String nacimiento = getVista().getTextFechaDeNacimiento().getText();
		clientes.add(new Persona(nombre+" "+apellido,genero.toUpperCase().charAt(0),mail,LocalDate.parse(nacimiento),contrasena));
		//Guardo datos en el archivo csv
		getListado().cargarArchivo(clientes);
		//agregar la accion del otro boton
	}
	JButton btn = (JButton) e.getSource();
	if(btn.getText().equals("Buscar")) {
		new ControladorVista2();
	}
}

public Vista1 getVista() {
	return vista;
}
public void setVista(Vista1 vista) {
	this.vista = vista;
}
public Listado getListado() {
	return listado;
}
public void setlistado(Listado listado) {
	this.listado = listado;
}

}
