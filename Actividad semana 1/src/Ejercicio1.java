//Alumno: Ojeda Fabricio
public class Ejercicio1 {

	public static void main(String[] args) {
		int numEntero = 3;
		double numFlotante = 2.3;
		char letra = 'A';
		String nombre = "Fabricio Ojeda";
		System.out.println("Mi nombre es "+nombre);
		
		//BUCLES
		System.out.println("Bucles para mostrar elementos de un vector");
		int [] vecEnteros = {0,1,2,3,4,5};
		int i = 0;
		System.out.println("En while");
		while (i < vecEnteros.length) {
			System.out.println(vecEnteros[i]);
			i++;
		}
		System.out.println("En for");
		for (int k=0; k< vecEnteros.length; k++) {
			System.out.println(vecEnteros[k]);
		}
		int j = 5;
		System.out.println("En do while (decrementando)");
		do{
			vecEnteros[j] = j;
			System.out.println(vecEnteros[j]);
			j--;
		}while (j>=0);
		
		//CONTROL
		System.out.println("Estructuras de control");
		switch (letra) {
		case 'A': System.out.println("La letra es "+letra);break;
		}
		letra='B';
		if (letra != 'A') {
			System.out.println("La letra ahora es "+letra);
		}
		//if corto
		System.out.println(numEntero > 1? "entero > 1": "entero <= 1");
	}
}
