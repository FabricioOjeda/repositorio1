import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.text.html.HTMLDocument.Iterator;

public class Reloj {
private HashMap<Personal, Integer> usuarios = new HashMap<Personal, Integer>();

public Reloj(HashMap<Personal, Integer> usuarios) {
	this.usuarios = usuarios;
}

public HashMap<Personal, Integer> getUsuarios() {
	return usuarios;
}

public void agregarUsuario (Personal persona) {
	SecureRandom num = new SecureRandom();
	usuarios.put(persona, num.nextInt(50));
}
public void informe() {
	java.util.Iterator<Entry<Personal, Integer>> it = this.usuarios.entrySet().iterator();
	while (it.hasNext()) {
		Map.Entry<Personal, Integer> entry = (Map.Entry<Personal, Integer>)it.next();
		System.out.println("Cantidad de horas de "+entry.getKey().getNombre()+": "+entry.getKey().calcularHorario(entry.getValue())+"hrs.");
		if (entry.getKey().getHoras() < entry.getKey().calcularHorario(entry.getValue())) {
			System.out.println("Se exedio de la cantidad de horas correspondientes por "+(entry.getKey().calcularHorario(entry.getValue())-entry.getKey().getHoras())+"hrs.");
		}
	}
}
}
