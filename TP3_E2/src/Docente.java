
public class Docente extends Personal {
	private Categoria categoria;
	public Docente(String nombre, String sector, Double antiguedad, Categoria categoria,Integer horas) {
		super(nombre, sector, antiguedad,horas);
		this.categoria = categoria;
	}

	@Override
	public Float calcularHorario(Integer cant) {
		switch (this.categoria) {
		case Simple: {this.setHoras(10);return (float) (10+(cant*0.95));}
		case Semiexclusiva: {this.setHoras(20);return (float) (20.0+cant*0.7);}
		case Exclusiva: {this.setHoras(40);return (float) (40+cant*0.6);}
		default:
			throw new IllegalArgumentException("Unexpected value: " + categoria);
		}
	}

	public Categoria getCategoria() {
		return categoria;
	}

}
