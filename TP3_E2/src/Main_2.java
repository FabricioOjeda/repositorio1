import java.util.HashMap;

public class Main_2 {

	public static void main(String[] args) {
		Docente docente1 = new Docente("Fabricio", "Informatica", 2.10, Categoria.Semiexclusiva,0);
		Docente docente2 = new Docente("Martin", "Informatica", 2.10, Categoria.Exclusiva,0);
		Reloj reloj = new Reloj(new HashMap<>());
		reloj.agregarUsuario(docente1);
		reloj.agregarUsuario(docente2);
		reloj.informe();
	}

}
