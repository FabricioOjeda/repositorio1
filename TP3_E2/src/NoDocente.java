
public class NoDocente extends Personal {
private JornadaLaboral jornada;
	public NoDocente(String nombre, String sector, Double antiguedad, JornadaLaboral jornada,Integer horas) {
		super(nombre, sector, antiguedad,horas);
		this.jornada = jornada;
	}

	@Override
	public Float calcularHorario(Integer cant) {
		switch (this.jornada) {
		case Completa: {this.setHoras(30);return (float) (30+cant*0.8); }
		case Reducida: {this.setHoras(20);return (float) (20+cant*0.8); }
		default:
			throw new IllegalArgumentException("Unexpected value: " + jornada);
		}
	}

	public JornadaLaboral getJornada() {
		return jornada;
	}

}
