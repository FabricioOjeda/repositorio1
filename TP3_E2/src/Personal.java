
public abstract class Personal {
private String nombre,sector;
private Double antiguedad;
private Integer horas;

public Personal(String nombre, String sector, Double antiguedad,Integer horas ) {
	this.nombre = nombre;
	this.sector = sector;
	this.antiguedad = antiguedad;
	this.horas = horas;
}

public String getNombre() {
	return nombre;
}

public String getSector() {
	return sector;
}

public Double getAntiguedad() {
	return antiguedad;
}

@Override
public String toString() {
	return "Nombre: "+this.getNombre()+" Sector: "+this.getSector()+" Antiguedad: "+this.getAntiguedad();
}
public abstract Float calcularHorario (Integer cant);

public Integer getHoras() {
	return horas;
}

public void setHoras(Integer horas) {
	this.horas = horas;
}

}
