
public class Leon extends Animal {

	public Leon(String nombre, String especie, String alimentacion) {
		super(nombre, especie, alimentacion);
	}

	@Override
	public String obtenerAlimento() {
		return "El leon obtiene alimento cazando";
	}

	@Override
	public String emitirSonido() {
		return "GROOOOOARGGGGGG";
	}

}
