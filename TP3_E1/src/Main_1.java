import java.util.ArrayList;

public class Main_1 {

	public static void main(String[] args) {
		Leon leon = new Leon("Leon","Mamifero","Carnivoro");
		Gato gato = new Gato("Gato", "Mamifero", "Carnivoro");
		Vaca vaca = new Vaca("Vaca", "Mamifero", "Hervivoro");
		ArrayList<Animal> animales = new ArrayList<Animal>();
		animales.add(leon);
		animales.add(vaca);
		animales.add(gato);
		for (Animal animal : animales) {
			System.out.println(animal.toString());
			System.out.println(animal.obtenerAlimento());
			System.out.println(animal.emitirSonido()+"\n\n");
		}
	}

}
