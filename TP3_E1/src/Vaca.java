
public class Vaca extends Animal {

	public Vaca(String nombre, String especie, String alimentacion) {
		super(nombre, especie, alimentacion);
	}

	@Override
	public String obtenerAlimento() {
		return "La vaca obitene alimento pastando";
	}

	@Override
	public String emitirSonido() {
		return "MUUUUU";
	}

}
