
public class Gato extends Animal {

	public Gato(String nombre, String especie, String alimentacion) {
		super(nombre, especie, alimentacion);
	}

	@Override
	public String obtenerAlimento() {
		return "El gato le dan alimento los due�os";
	}

	@Override
	public String emitirSonido() {
		return "MEOW";
	}

}
