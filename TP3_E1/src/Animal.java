
public abstract class Animal {
private String nombre,especie,alimentacion;

public Animal(String nombre, String especie, String alimentacion) {
	this.nombre = nombre;
	this.especie = especie;
	this.alimentacion = alimentacion;
}

//tal vez quitar los sets
public String getNombre() {
	return nombre;
}

public String getEspecie() {
	return especie;
}

public String getAlimentacion() {
	return alimentacion;
}

public abstract String obtenerAlimento();
public abstract String emitirSonido();

@Override
public String toString() {
	return "Animal: "+this.getNombre()+"\nEspecie: "+this.getEspecie()+"\nAlimentacion:"+this.getAlimentacion();
}

}
