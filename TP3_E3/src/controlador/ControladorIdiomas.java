package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import modelo.Frances;
import modelo.Ingles;
import modelo.Portugues;
import vusta.VistaIdiomas;

public class ControladorIdiomas implements ActionListener {
	private VistaIdiomas vista;
	
	
	public ControladorIdiomas() {
		this.vista = new VistaIdiomas(this);
		this.vista.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(getVista().getBtnIngles())) {
			Ingles ingles = new Ingles();
			new ControladorOpciones(ingles);
			/*
			System.out.println("Idioma: "+ingles.getClass().getName());
			System.out.println(ingles.saludar());
			System.out.println(ingles.despedirse());
			System.out.println(ingles.perdon());
			System.out.println(ingles.pedirCafe());
			System.out.println(ingles.cuantoCuesta());
			System.out.println(ingles.dondeQueda()+"\n\n");
			*/
		}
		if(e.getSource().equals(getVista().getBtnFrances())) {
			Frances frances = new Frances();
			new ControladorOpciones(frances);
			/*
			System.out.println("Idioma: "+frances.getClass().getName());
			System.out.println(frances.saludar());
			System.out.println(frances.despedirse());
			System.out.println(frances.perdon());
			System.out.println(frances.pedirCafe());
			System.out.println(frances.cuantoCuesta());
			System.out.println(frances.dondeQueda()+"\n\n");
			*/
		}
		if(e.getSource().equals(getVista().getBtnPortugues())) {
			Portugues portugues = new Portugues();
			new ControladorOpciones(portugues);
			/*
			System.out.println("Idioma: "+portugues.getClass().getName());
			System.out.println(portugues.saludar());
			System.out.println(portugues.despedirse());
			System.out.println(portugues.perdon());
			System.out.println(portugues.pedirCafe());
			System.out.println(portugues.cuantoCuesta());
			System.out.println(portugues.dondeQueda()+"\n\n");
			*/
		}
	}

	public VistaIdiomas getVista() {
		return vista;
	}

}
