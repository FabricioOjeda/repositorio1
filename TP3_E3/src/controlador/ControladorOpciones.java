package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import modelo.Idioma;
import vusta.VistaMensajes;

public class ControladorOpciones implements ActionListener {
	private VistaMensajes vista;
	private Idioma idioma;
	//como pasar por parametro el idioma que se selecciono????
	public ControladorOpciones(Idioma idioma) {
		this.vista = new VistaMensajes(this);
		this.vista.setVisible(true);
		this.idioma = idioma;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(getVista().getBtnSaludar())) {
			JOptionPane.showMessageDialog(null, idioma.saludar());
		}
		if(e.getSource().equals(getVista().getBtnDespedirse())) {
			JOptionPane.showMessageDialog(null,idioma.despedirse());
		}
		if(e.getSource().equals(getVista().getBtnPerdon())) {
			JOptionPane.showMessageDialog(null,idioma.perdon());
		}
		if(e.getSource().equals(getVista().getBtnCafe())) {
			JOptionPane.showMessageDialog(null,idioma.pedirCafe());
		}
		if(e.getSource().equals(getVista().getBtnCosto())) {
			JOptionPane.showMessageDialog(null,idioma.cuantoCuesta());
		}
		if(e.getSource().equals(getVista().getBtnUbicacion())) {
			JOptionPane.showMessageDialog(null,idioma.dondeQueda());
		}
	}


	public VistaMensajes getVista() {
		return vista;
	}

	public void setVista(VistaMensajes vista) {
		this.vista = vista;
	}
}
