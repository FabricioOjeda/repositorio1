package modelo;

public class Ingles implements Idioma {

	@Override
	public String saludar() {
		return "Hello there";
	}

	@Override
	public String despedirse() {
		return "Bye";
	}

	@Override
	public String perdon() {
		return "Sorry";
	}

	@Override
	public String pedirCafe() {
		return "A coffe please";
	}

	@Override
	public String cuantoCuesta() {
		return "How much is it?";
	}

	@Override
	public String dondeQueda() {
		return "Where is?";
	}

}
