package modelo;

import java.awt.EventQueue;

import controlador.ControladorIdiomas;

public class Main_Idiomas {
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new ControladorIdiomas();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
