import java.util.ArrayList;

import modelo.Frances;
import modelo.Idioma;
import modelo.Ingles;
import modelo.Portugues;

public class Main_3 {

	public static void main(String[] args) {
		Ingles ingles = new Ingles();
		Portugues portu = new Portugues();
		Frances frances = new Frances();
		ArrayList<Idioma> idiomas = new ArrayList<Idioma>();
		idiomas.add(ingles);
		idiomas.add(portu);
		idiomas.add(frances);
		for (Idioma idioma : idiomas) {
			System.out.println("Idioma: "+idioma.getClass().getName());
			System.out.println(idioma.saludar());
			System.out.println(idioma.despedirse());
			System.out.println(idioma.perdon());
			System.out.println(idioma.pedirCafe());
			System.out.println(idioma.cuantoCuesta());
			System.out.println(idioma.dondeQueda()+"\n\n");
		}
	}

}
