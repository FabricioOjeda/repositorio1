package vusta;

//import java.awt.BorderLayout;
//import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorIdiomas;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JButton;
//import java.awt.event.ActionListener;
//import java.awt.event.ActionEvent;

public class VistaIdiomas extends JFrame {

	private JPanel contentPane;
	private ControladorIdiomas controlador;
	private JButton btnIngles;
	private JButton btnFrances;
	private JButton btnPortugues;
	
	public VistaIdiomas(ControladorIdiomas controlador) {
		this.setControlador(controlador);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 363, 236);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSeleccionIdioma = new JLabel("Seleccione el idioma");
		lblSeleccionIdioma.setHorizontalAlignment(SwingConstants.CENTER);
		lblSeleccionIdioma.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblSeleccionIdioma.setBounds(10, 11, 327, 28);
		contentPane.add(lblSeleccionIdioma);
		
		btnIngles = new JButton("Ingles");
		btnIngles.addActionListener(this.getControlador());
		btnIngles.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnIngles.setBounds(117, 50, 108, 23);
		contentPane.add(btnIngles);
		
		btnFrances = new JButton("Frances");
		btnFrances.addActionListener(this.getControlador());
		btnFrances.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnFrances.setBounds(117, 84, 108, 23);
		contentPane.add(btnFrances);
		
		btnPortugues = new JButton("Portugues");
		btnPortugues.addActionListener(this.getControlador());
		btnPortugues.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnPortugues.setBounds(117, 121, 108, 23);
		contentPane.add(btnPortugues);
	}

	public ControladorIdiomas getControlador() {
		return controlador;
	}

	public void setControlador(ControladorIdiomas controlador) {
		this.controlador = controlador;
	}

	public JButton getBtnIngles() {
		return btnIngles;
	}

	public JButton getBtnFrances() {
		return btnFrances;
	}

	public JButton getBtnPortugues() {
		return btnPortugues;
	}
}
