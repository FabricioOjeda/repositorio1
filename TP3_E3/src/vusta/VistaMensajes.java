package vusta;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorOpciones;

import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VistaMensajes extends JFrame {

	private JPanel contentPane;
	private JButton btnSaludar;
	private JButton btnDespedirse;
	private JButton btnPerdon;
	private JButton btnCafe;
	private JButton btnCosto;
	private JButton btnUbicacion;
	private ControladorOpciones controlador;

	public VistaMensajes(ControladorOpciones controlador) {
		this.setControlador(controlador);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 300, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnSaludar = new JButton("Saludar");
		btnSaludar.addActionListener(this.getControlador());
		btnSaludar.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnSaludar.setBounds(74, 30, 131, 23);
		contentPane.add(btnSaludar);
		
		btnDespedirse = new JButton("Despedirse");
		btnDespedirse.addActionListener(this.getControlador());
		btnDespedirse.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnDespedirse.setBounds(74, 64, 131, 23);
		contentPane.add(btnDespedirse);
		
		btnPerdon = new JButton("Perdon");
		btnPerdon.addActionListener(this.getControlador());
		btnPerdon.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnPerdon.setBounds(74, 98, 131, 23);
		contentPane.add(btnPerdon);
		
		btnCafe = new JButton("Cafe");
		btnCafe.addActionListener(this.getControlador());
		btnCafe.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnCafe.setBounds(74, 132, 131, 23);
		contentPane.add(btnCafe);
		
		btnCosto = new JButton("Costo");
		btnCosto.addActionListener(this.getControlador());
		btnCosto.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnCosto.setBounds(74, 166, 131, 23);
		contentPane.add(btnCosto);
		
		btnUbicacion = new JButton("Ubicacion");
		btnUbicacion.addActionListener(this.getControlador());
		btnUbicacion.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnUbicacion.setBounds(74, 200, 131, 23);
		contentPane.add(btnUbicacion);
	}

	public ControladorOpciones getControlador() {
		return controlador;
	}

	public void setControlador(ControladorOpciones controlador) {
		this.controlador = controlador;
	}

	public JButton getBtnSaludar() {
		return btnSaludar;
	}

	public JButton getBtnDespedirse() {
		return btnDespedirse;
	}

	public JButton getBtnPerdon() {
		return btnPerdon;
	}

	public JButton getBtnCafe() {
		return btnCafe;
	}

	public JButton getBtnCosto() {
		return btnCosto;
	}

	public JButton getBtnUbicacion() {
		return btnUbicacion;
	}

}
