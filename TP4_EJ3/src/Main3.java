import java.awt.EventQueue;

import controlador.ControladorFormulario;
import controlador.ControladorLogin;
import controlador.ControladorStock;

public class Main3 {
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//new ControladorLogin();
					//new ControladorStock();
					new ControladorFormulario();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
