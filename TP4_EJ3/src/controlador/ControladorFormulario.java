package controlador;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.regex.Pattern;

import javax.swing.ButtonGroup;
import javax.swing.border.LineBorder;

import vista.VistaFormulario;

public class ControladorFormulario implements KeyListener,ActionListener, FocusListener{
	private VistaFormulario vistaFormulario;
	
	public ControladorFormulario () {
		this.vistaFormulario = new VistaFormulario(this);
		this.vistaFormulario.setVisible(true);
		ButtonGroup bg = new ButtonGroup();
		bg.add(this.getVistaFormulario().getRdbtnCelular());
		bg.add(this.getVistaFormulario().getRdbtnFijo());
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getSource().equals(this.getVistaFormulario().getTextFieldTelefono())) {
			validarNumeros();
		}
	}
	
	public void validarNumeros () {
		 String texto = this.getVistaFormulario().getTextFieldTelefono().getText();
	     ArrayList<Character> listTexto = new ArrayList<Character>();
	     char[] charTexto = texto.toCharArray();
	     for(char c : charTexto) {
	            if (esNumero(String.valueOf(c))) {
	            	listTexto.add(c);
				}
	     }
	     texto = "";
	     for (char n : listTexto) {
			texto += n;
		}
	     this.getVistaFormulario().getTextFieldTelefono().setText(texto);
	}
	
	public Boolean esNumero(String n) {
		try {
			Integer.parseInt(n);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	public void validarMail () {
		//Regular Expression   
	    String regx = "^[A-Za-z0-9+_.-]+@(.+)$";  
	    //Compile regular expression to get the pattern  
	    Pattern pattern = Pattern.compile(regx);  
	    java.util.regex.Matcher matcher = pattern.matcher(this.getVistaFormulario().getTextFieldEmail().getText());
	    if (!matcher.matches()) {
			this.getVistaFormulario().getTextFieldEmail().setText("");
		}
	}

	public void validarContrasena() {
		char[] contrasena = this.getVistaFormulario().getPasswordField().getPassword();
		// >= 8 digitos & alguna letra mayuscula & algun numero
		if (contrasena.length <8 || !hasUpper(contrasena) || !hasNumber(contrasena)) {
			this.getVistaFormulario().getPasswordField().setText("");
		} 
	}
	
	public Boolean hasUpper (char[] ch) {
		for (char letra : ch) {
			if (Character.isUpperCase(letra)) {
				return true;
			}
		}
		 return false;
	}
	
	public Boolean hasNumber (char[] ch) {
		for (char letra : ch) {
			if (Character.isDigit(letra)) {
				return true;
			}
		}
		 return false;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(this.getVistaFormulario().getBtnGuardar())) {
			validarMail();
			validarContrasena();
		}
		
	}

	@Override
	public void focusGained(FocusEvent e) {
		// TODO Auto-generated method stub
		//CUANDO GANA EL FOCO SE MARCA EL TEXTFIELD EN NEGRO
		if (e.getSource().equals(this.getVistaFormulario().getTextFieldUsuario())) {
			this.getVistaFormulario().getTextFieldUsuario().setBorder(new LineBorder(Color.BLACK));
		}
		if (e.getSource().equals(this.getVistaFormulario().getTextFieldEmail())) {
			this.getVistaFormulario().getTextFieldEmail().setBorder(new LineBorder(Color.BLACK));
		}
		if (e.getSource().equals(this.getVistaFormulario().getPasswordField())) {
			this.getVistaFormulario().getPasswordField().setBorder(new LineBorder(Color.BLACK));
		}
	}

	@Override
	public void focusLost(FocusEvent e) {
		// TODO Auto-generated method stub
		//SI PIERDEN EL FOCO Y ESTAN VACIOS SE MARCA EL TEXTFIELD EN ROJO Y SE MUESTRA EL MENSAJE
		//SINO SE MARCA EN VERDE Y SE OCULTA EL MENSAJE
		if (e.getSource().equals(this.getVistaFormulario().getTextFieldUsuario())) {
			if (this.getVistaFormulario().getTextFieldUsuario().getText().equals("")) {
				this.getVistaFormulario().getTextFieldUsuario().setBorder(new LineBorder(Color.RED));
				this.getVistaFormulario().getLblErrorUsuario().setVisible(true);
			} else {
				this.getVistaFormulario().getTextFieldUsuario().setBorder(new LineBorder(Color.GREEN));
				this.getVistaFormulario().getLblErrorUsuario().setVisible(false);
			}
		}
		if (e.getSource().equals(this.getVistaFormulario().getTextFieldEmail())) {
			if (this.getVistaFormulario().getTextFieldEmail().getText().equals("")) {
				this.getVistaFormulario().getTextFieldEmail().setBorder(new LineBorder(Color.RED));
				this.getVistaFormulario().getLblErrorEmail().setVisible(true);
			} else {
				this.getVistaFormulario().getTextFieldEmail().setBorder(new LineBorder(Color.GREEN));
				this.getVistaFormulario().getLblErrorEmail().setVisible(false);
			}
		}
		if (e.getSource().equals(this.getVistaFormulario().getPasswordField())) {
			this.getVistaFormulario().getPasswordField().setBorder(new LineBorder(Color.GREEN));
		}
	}
	public VistaFormulario getVistaFormulario() {
		return vistaFormulario;
	}
}
