package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorFormulario;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTabbedPane;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import java.awt.Color;

public class VistaFormulario extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldEmail;
	private JTextField textFieldTelefono;
	private JLabel lblUsuario;
	private JTextField textFieldUsuario;
	private JComboBox comboBox;
	private ControladorFormulario controladorFormulario;
	private JRadioButton rdbtnFijo;
	private JRadioButton rdbtnCelular;
	private JButton btnGuardar;
	private JPasswordField passwordField;
	private JLabel lblErrorEmail;
	private JLabel lblErrorUsuario;
	private JLabel lblErrorContrasena;
	/**
	 * Create the frame.
	 */
	public VistaFormulario(ControladorFormulario controladorFormulario) {
		this.setControladorFormulario(controladorFormulario);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 413, 485);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Formulario");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel.setBounds(153, 27, 98, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblEmail = new JLabel("*Email:");
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblEmail.setBounds(52, 65, 52, 14);
		contentPane.add(lblEmail);
		
		textFieldEmail = new JTextField();
		textFieldEmail.addFocusListener(getControladorFormulario());
		textFieldEmail.setBounds(163, 62, 180, 20);
		contentPane.add(textFieldEmail);
		textFieldEmail.setColumns(10);
		
		textFieldTelefono = new JTextField();
		textFieldTelefono.addKeyListener(this.getControladorFormulario());
		textFieldTelefono.setColumns(10);
		textFieldTelefono.setBounds(163, 123, 180, 20);
		contentPane.add(textFieldTelefono);
		
		JLabel lblTelefono = new JLabel("Telefono:");
		lblTelefono.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblTelefono.setBounds(52, 126, 74, 14);
		contentPane.add(lblTelefono);
		
		textFieldUsuario = new JTextField();
		textFieldUsuario.addFocusListener(getControladorFormulario());
		textFieldUsuario.setColumns(10);
		textFieldUsuario.setBounds(163, 234, 180, 20);
		contentPane.add(textFieldUsuario);
		
		lblUsuario = new JLabel("*Usuario:");
		lblUsuario.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblUsuario.setBounds(52, 237, 74, 14);
		contentPane.add(lblUsuario);
		
		JLabel lblGenero = new JLabel("Genero:");
		lblGenero.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblGenero.setBounds(52, 189, 74, 14);
		contentPane.add(lblGenero);
		
		comboBox = new JComboBox();
		comboBox.setFont(new Font("Tahoma", Font.PLAIN, 15));
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Indefinido", "Masculino", "Femenino"}));
		comboBox.setBounds(163, 187, 180, 22);
		contentPane.add(comboBox);
		
		rdbtnFijo = new JRadioButton("Fijo");
		rdbtnFijo.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbtnFijo.setBounds(167, 155, 66, 23);
		contentPane.add(rdbtnFijo);
		
		rdbtnCelular = new JRadioButton("Celular");
		rdbtnCelular.setFont(new Font("Tahoma", Font.PLAIN, 15));
		rdbtnCelular.setBounds(247, 155, 90, 23);
		contentPane.add(rdbtnCelular);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnGuardar.setBounds(52, 412, 291, 23);
		btnGuardar.addActionListener(this.getControladorFormulario());
		contentPane.add(btnGuardar);
		
		JLabel lblContrasena = new JLabel("*Contrasena:");
		lblContrasena.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblContrasena.setBounds(52, 293, 90, 14);
		contentPane.add(lblContrasena);
		
		passwordField = new JPasswordField();
		passwordField.addFocusListener(getControladorFormulario());
		passwordField.setBounds(163, 292, 180, 20);
		contentPane.add(passwordField);
		
		JLabel lblNewLabel_1 = new JLabel("* campos obligatorios");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(208, 387, 146, 14);
		contentPane.add(lblNewLabel_1);
		
		lblErrorEmail = new JLabel("Ingrese datos porfavor");
		lblErrorEmail.setForeground(Color.RED);
		lblErrorEmail.setBounds(173, 93, 170, 14);
		contentPane.add(lblErrorEmail);
		
		lblErrorUsuario = new JLabel("Ingrese datos porfavor");
		lblErrorUsuario.setForeground(Color.RED);
		lblErrorUsuario.setBounds(173, 265, 170, 14);
		contentPane.add(lblErrorUsuario);
		
		lblErrorContrasena = new JLabel("Ingrese datos porfavor");
		lblErrorContrasena.setForeground(Color.RED);
		lblErrorContrasena.setBounds(173, 323, 170, 14);
		contentPane.add(lblErrorContrasena);
		
		//PARA OCULTAR LOS LBL DE MENSAJES DE ERRORES
		this.getLblErrorContrasena().setVisible(false);
		this.getLblErrorUsuario().setVisible(false);
		this.getLblErrorEmail().setVisible(false);
	}
	public ControladorFormulario getControladorFormulario() {
		return controladorFormulario;
	}
	public void setControladorFormulario(ControladorFormulario controladorFormulario) {
		this.controladorFormulario = controladorFormulario;
	}
	public JTextField getTextFieldEmail() {
		return textFieldEmail;
	}
	public JTextField getTextFieldTelefono() {
		return textFieldTelefono;
	}
	public JLabel getLblUsuario() {
		return lblUsuario;
	}
	public JTextField getTextFieldUsuario() {
		return textFieldUsuario;
	}
	public JRadioButton getRdbtnCelular() {
		return rdbtnCelular;
	}
	public JRadioButton getRdbtnFijo() {
		return rdbtnFijo;
	}
	public JComboBox getComboBox() {
		return comboBox;
	}
	public JPasswordField getPasswordField() {
		return passwordField;
	}
	public JLabel getLblErrorEmail() {
		return lblErrorEmail;
	}
	public void setLblErrorEmail(JLabel lblErrorEmail) {
		this.lblErrorEmail = lblErrorEmail;
	}
	public JLabel getLblErrorUsuario() {
		return lblErrorUsuario;
	}
	public void setLblErrorUsuario(JLabel lblErrorUsuario) {
		this.lblErrorUsuario = lblErrorUsuario;
	}
	public JLabel getLblErrorContrasena() {
		return lblErrorContrasena;
	}
	public void setLblErrorContrasena(JLabel lblErrorContrasena) {
		this.lblErrorContrasena = lblErrorContrasena;
	}
	public void setTextFieldTelefono(JTextField textFieldTelefono) {
		this.textFieldTelefono = textFieldTelefono;
	}
	public JButton getBtnGuardar() {
		return btnGuardar;
	}
}
