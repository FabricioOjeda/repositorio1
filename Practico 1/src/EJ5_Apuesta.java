import java.security.SecureRandom;
public class EJ5_Apuesta {
//A
	SecureRandom ran = new SecureRandom ();
	private Integer num1,num2,ganados,perdidos;
//C
public EJ5_Apuesta () {
	this.setNum1(-1);
	this.setNum2(-1);
	this.setGanados(0);
	this.setPerdidos(0);
}
public EJ5_Apuesta (Integer num1, Integer num2, Integer ganados, Integer perdidos) {
	this.setNum1(num1);
	this.setNum2(num2);
	this.setGanados(ganados);
	this.setPerdidos(perdidos);
}
//M
	public Integer getNum1() {
		return num1;
	}
	private void setNum1(Integer num1) {
		num1 = ran.nextInt(10)+1;
		this.num1 = num1;
	}
	public Integer getNum2() {
		return num2;
	}
	private void setNum2(Integer num2) {
		num2 = ran.nextInt(10)+1;
		this.num2 = num2;
	}
	public Integer getGanados() {
		return ganados;
	}
	private void setGanados(Integer ganados) {
		this.ganados = ganados;
	}
	public Integer getPerdidos() {
		return perdidos;
	}
	private void setPerdidos(Integer perdidos) {
		this.perdidos = perdidos;
	}
	public  void resultado (Integer numero) {
		if ((numero == 1 & this.num1 > this.num2) | (numero == 2 & this.num2 > this.num1)) {
			System.out.println("Usted gano");
			this.ganados++;
		} else {
			System.out.println("Usted perdio");
			this.perdidos++;
		}
	}
	public void mezclar () {
		this.setNum1(num1);
		this.setNum2(num2);
	}
}