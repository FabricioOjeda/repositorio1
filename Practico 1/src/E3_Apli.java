
import java.util.Scanner;
public class E3_Apli {

	public static void main(String[] args) {
		E3_Random numeros = new E3_Random();
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Ingrese el primer numero");
		numeros.setNum1(entrada.nextInt());
		if (numeros.getNum1() != numeros.getNum2()) {
			System.out.println(numeros.getNum1()+" es distinto de " +numeros.getNum2());	
		}
		if (numeros.getNum1() < numeros.getNum2()) {
			System.out.println(numeros.getNum1()+" es menor a "+numeros.getNum2());
		} else {
			System.out.println(numeros.getNum1()+" es mayor a "+numeros.getNum2());
		}
	}

}
