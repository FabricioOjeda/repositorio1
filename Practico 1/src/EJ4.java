import java.util.Scanner;
public class EJ4 {
	public static void main(String[] args) {
		Scanner entrada = new Scanner (System.in);
		String cade1,cade2;
		System.out.println("Primera palabra: ");
		cade1 = entrada.next();
		System.out.println("Segunda palabra: ");
		cade2 = entrada.next();
		if (cade1 == cade2) {
			System.out.println("Comparado con == : true");
		} else {
			System.out.println("Comparado con == : false");
		}
		if (cade1.equalsIgnoreCase(cade2)) {
			System.out.println("Comparado con .equals() : true");
		} else {
			System.out.println("Comparado con .equals() : false");
		}
		//Para comparar cadenas usar el metodo .equals
	}
}
