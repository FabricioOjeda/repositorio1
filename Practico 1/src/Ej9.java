import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;

public class Ej9 {
		public static void main(String[] args) {
			try (Scanner input = new Scanner(Paths.get("Archivos/File1.txt"))){
				String palabra;
				Integer cantidad = 0;
				while(input.hasNext()) {
					palabra = input.next();
					cantidad = cantidad + palabra.length();
				}
				System.out.println("Cantidad de letras "+cantidad);
			} catch (IOException e) {
				e.printStackTrace();
			}
	}

}
