import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;

public class EJ10 {

		public static void main(String[] args) {
			try (Scanner input = new Scanner(Paths.get("Archivos/ArchivoCSV.csv"))){
				input.useDelimiter("\\r\\n|\\n\\r");	
				String[] datos;
				while(input.hasNext()) {
					datos = input.next().split(";");
					System.out.println("Nombre: "+datos[0]);
					System.out.println("Apellido: "+datos[1]);
					System.out.println("DNI: "+Integer.valueOf(datos[2]));
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

	}

}
