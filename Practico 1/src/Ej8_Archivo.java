import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Formatter;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Ej8_Archivo {

	public static void main(String[] args) {
		try (Formatter output = new Formatter ("Archivos/File1.txt")){
			Scanner input = new Scanner(System.in);
			System.out.printf("%s%n%sn?","Ingrese nombre, apellido y edad", "Ingrese ctrl+z para finalizar.");
			while(input.hasNext()) {
				try {
				output.format("%s %s %d%n",input.next(), input.next(),input.nextInt());
			}
				catch(NoSuchElementException elementException) {
					System.err.println("El valor ingresado no es valido");
					input.nextLine();
				}
				System.out.println("?");
				}
			
			}  catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		try (Scanner input = new Scanner(Paths.get("Archivos/File1.txt"))){
			System.out.printf("%-10s%-12s%-12s%n", "nombre","apellido","edad");
			while(input.hasNext()) {
				System.out.printf("%-12s%-12s%-12d%n", input.next(), input.next(), input.nextInt());
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
