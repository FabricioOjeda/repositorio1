
public class E2_Texto {
	//Atributos
	private String palabra1,palabra2,juntas;
	//Constructores
	public E2_Texto (){
	this.setPalabra1("");
	this.setPalabra2("");
	this.setJuntas("");
	}
	public E2_Texto (String palabra1, String palabra2, String juntas){
		this.setPalabra1(palabra1);
		this.setPalabra2(palabra2);
		this.setJuntas(juntas);
		}
	//Metodos
	public String getPalabra1() {
		return palabra1;
	}
	public void setPalabra1(String palabra1) {
		this.palabra1 = palabra1;
	}
	public String getPalabra2() {
		return palabra2;
	}
	public void setPalabra2(String palabra2) {
		this.palabra2 = palabra2;
	}
	public String getJuntas() {
		return palabra1+" "+palabra2;
	}
	public void setJuntas(String juntas) {
		this.juntas = juntas;
	}
}
