 import java.security.SecureRandom;
public class E3_Random {

	//A
	private Integer num1,num2;
	private SecureRandom ran = new SecureRandom();
//C
	public E3_Random () {
		this.setNum1(-1);
		this.setNum2(-1);
	}
	public E3_Random (Integer num1, Integer num2) {
		this.setNum1(num1);
		this.setNum2(num2);
	}	
//M
	public Integer getNum1() {
		return num1;
	}
	public void setNum1(Integer num1) {
		this.num1 = num1;
	}
	public Integer getNum2() {
		return num2;
	}
	public void setNum2(Integer num2) {
		num2 = ran.nextInt(10);
		this.num2 = num2;
	}
	
}

