import java.util.Scanner;
public class EJ5_Apli {
	public static void main(String[] args) {
		EJ5_Apuesta apuesta = new EJ5_Apuesta();
		Scanner entrada = new Scanner(System.in);
		Character rta;
		Integer nro_apuesta; 
		
		do {
			apuesta.mezclar();
		while (apuesta.getNum1() == apuesta.getNum2() ) {
			apuesta.mezclar();
		} 
		System.out.println("A que numero desea apostar? [1] | [2]");
		nro_apuesta = entrada.nextInt();
		apuesta.resultado(nro_apuesta);
		System.out.println("Numero 1: "+apuesta.getNum1());
		System.out.println("Numero 2: "+apuesta.getNum2());
		System.out.println("Volver a jugar? [S] | [N]");
		rta = entrada.next().charAt(0);
		}while (rta == 'S' | rta == 's');
		
		System.out.println("Usted gano "+apuesta.getGanados()+" juegos");
		System.out.println("Usted perdio "+apuesta.getPerdidos()+" juegos");
	}

}