package modelo;

import java.time.LocalDate;

public class Persona {
	private String nombre, mail,contrasena,fechaNacimiento;
	private Character genero;
	//private LocalDate fechaNacimiento;
//C
public Persona() {
		this("",'a',"","","");
	}
	public Persona(String nombre, Character genero, String mail, String fechaNacimiento, String contrasena) {
	this.setNombre(nombre);
	this.setGenero(genero);
	this.setMail(mail);
	this.setFechaNacimiento(fechaNacimiento);
	this.setContrasena(contrasena);
}

	//M
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Character getGenero() {
		return genero;
	}
	public void setGenero(Character c) {
		this.genero = c;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public void imprimirInfo (Persona unaPersona) {
		System.out.println("Nombre: "+ this.getNombre());
		System.out.println("Fecha de nacimiento: "+ this.getFechaNacimiento());
		System.out.println("Genero: "+ this.getGenero());
		System.out.println("Email: "+this.getMail());
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
}
