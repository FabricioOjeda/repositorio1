package modelo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Map.Entry;

public class Banco {
	private String nombre; //nombre del banco
	private HashMap<Persona, Double> cuentas = new HashMap<Persona,Double>();

	//C
	public Banco() {
	this("",new HashMap<Persona,Double>());
	//aca se podria crear el archivo
	}

	public Banco(String nombre, HashMap<Persona, Double> cuentas) {
		this.setNombre(nombre);
		this.setCuentas(cuentas);
	}

	//M
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public HashMap<Persona, Double> getCuentas() {
		return cuentas;
	}
	public void setCuentas(HashMap<Persona, Double> cuentas) {
		this.cuentas = cuentas;
	}
	//Metodo para agregar una cuenta
	//Tambien podria usarse para actualizar el archivo
	public Boolean agregarCuenta(Persona persona, Double saldo) {
		this.setNombre(persona.getNombre());
		cuentas.put(persona, saldo);
		return true;
	}
	public ArrayList<Persona> leerArchivo (ArrayList<Persona> datos){
		//ArrayList<Persona> datos = new ArrayList<Persona>();
		//bajo archivo csv
		try (Scanner input = new Scanner(Paths.get("archivos/cuentas.csv"))){
			while(input.hasNext()) {
				Persona unapersona = new Persona(input.next()+" "+input.next(),input.next().charAt(0), input.next(),input.next(),input.next());
				datos.add(unapersona);
				agregarCuenta(unapersona, 0.0);
			}
		} catch (IOException a) {
			a.printStackTrace();
		}
		return datos;
	}
	public void cargarArchivo (ArrayList<Persona> datos) {
		try (Formatter output = new Formatter ("archivos/cuentas.csv")){
			for (Persona persona : datos) {
				output.format("%s %c %s %s %s%n",persona.getNombre(),persona.getGenero(),persona.getMail(),persona.getFechaNacimiento(),persona.getContrasena());
			}
		}  catch (FileNotFoundException exc) {
			exc.printStackTrace();
		}
	}
	//Metodo para eliminar una cuenta
	//Tambien podria usarse para actualizar el archivo
	public Boolean eliminarCuenta(Persona persona) {
		if (this.getCuentas().containsKey(persona)) {
			this.getCuentas().remove(persona);
			return true;
		}
		return false;
	}

	//Metodo para imprimir clientes del banco
	public void listarClientes () {
		java.util.Iterator<Entry<Persona, Double>> it = this.getCuentas().entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Persona, Double> entry = (Entry<Persona, Double>)it.next();
			Persona una = new Persona();
			una = entry.getKey();
			System.out.println("Nombre: "+una.getNombre()+ " saldo: "+entry.getValue());
		} 
	}
	public Double getSaldo (Persona persona) {
		java.util.Iterator<Entry<Persona, Double>> it = this.getCuentas().entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Persona, Double> entry = (Entry<Persona, Double>)it.next();
			Persona una = new Persona();
			una = entry.getKey();
			if (una.getNombre().equals(persona.getNombre())) {
				return entry.getValue();
			}
		}
		return 0.0; 
		}
}
