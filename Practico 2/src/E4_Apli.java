import java.time.LocalDate;
import java.util.Scanner;

public class E4_Apli {

	public static void main(String[] args) {
		E4_Banco banco = new E4_Banco();
		E1_Persona tesla = new E1_Persona("Nikola Tesla",'M',"NTesla@unEmail.com",LocalDate.of(1856, 07, 10),"alterna");
		E1_Persona edison = new E1_Persona("Thomas Edison",'M',"TEdison@unEmail.com",LocalDate.of(1875, 11, 13),"tomiedison123");
		E1_Persona gosling = new E1_Persona("James Gosling",'M',"JGosling@unEmail.com",LocalDate.of(1900, 03, 07),"asd123");
		banco.agregarCuenta(tesla, 1208.8);
		banco.agregarCuenta(edison, 5135.8);
		banco.agregarCuenta(gosling, 153.8);
		banco.listarClientes();
		System.out.println("Saldo de "+tesla.getNombre()+": "+banco.getSaldo(tesla));
		if (banco.eliminarCuenta(gosling)) {
			System.out.println("Se borro a "+gosling.getNombre());
		}else {
			System.out.println("No se elimino");
		}
		System.out.println("------------------------------");
		banco.listarClientes();
		System.out.println("Saldo de "+gosling.getNombre()+": "+banco.getSaldo(gosling));
		/*Scanner input = new Scanner(System.in);
		System.out.println("Nombre: ");
		persona1.setNombre(input.nextLine());
		System.out.println("Fecha de nacimiento");
		persona1.setFechaNacimiento(LocalDate.of(input.nextInt(), input.nextInt(), input.nextInt()));
		System.out.println("Genero: ");
		persona1.setGenero(input.next().charAt(0));
		System.out.println("Email: ");
		persona1.setMail(input.nextLine());
		System.out.println("Contraseņa: ");
		persona1.setContrasena(input.nextLine());
		banco.agregarCuenta(persona1, 1208.8);
		
		E1_Persona persona2 = new E1_Persona();
		System.out.println("Nombre: ");
		persona2.setNombre(input.nextLine());
		System.out.println("Fecha de nacimiento");
		persona2.setFechaNacimiento(LocalDate.of(input.nextInt(), input.nextInt(), input.nextInt()));
		System.out.println("Genero: ");
		persona2.setGenero(input.next().charAt(0));
		System.out.println("Email: ");
		persona2.setMail(input.nextLine());
		System.out.println("Contraseņa: ");
		persona2.setContrasena(input.nextLine());
		banco.agregarCuenta(persona2, 5135.8);*/
		/*banco.listarClientes();
		if (banco.eliminarCuenta(persona2)) {
			System.out.println("Se borro a "+persona2.getNombre());
		}else {
			System.out.println("No se elimino");
		}*/

	}

}
