package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorPunto5;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VistaPunto5 extends JFrame {

	private JPanel contentPane;
	private JTextField textNombre;
	private JLabel lblApellido;
	private JTextField textApellido;
	private JLabel lblEmail;
	private JTextField textEmail;
	private JLabel lblGenero;
	private JTextField textGenero;
	private JLabel lblContraseña;
	private JTextField textContraseña;
	private JLabel lblFechaDeNacimiento;
	private JTextField textFechaDeNacimiento;
	private ControladorPunto5 controlador;
	private JButton btnGuardar;


	/**
	 * Create the frame.
	 */
	public VistaPunto5(ControladorPunto5 controlador) {
		this.setControlador(controlador);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 540);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNuevaCuenta = new JLabel("Nueva Cuenta");
		lblNuevaCuenta.setHorizontalAlignment(SwingConstants.CENTER);
		lblNuevaCuenta.setFont(new Font("Tahoma", Font.PLAIN, 21));
		lblNuevaCuenta.setBounds(211, 11, 158, 61);
		contentPane.add(lblNuevaCuenta);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setHorizontalAlignment(SwingConstants.LEFT);
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNombre.setBounds(27, 81, 92, 38);
		contentPane.add(lblNombre);
		
		textNombre = new JTextField();
		textNombre.setBounds(236, 94, 270, 20);
		contentPane.add(textNombre);
		textNombre.setColumns(10);
		
		lblApellido = new JLabel("Apellido:");
		lblApellido.setHorizontalAlignment(SwingConstants.LEFT);
		lblApellido.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblApellido.setBounds(27, 130, 92, 38);
		contentPane.add(lblApellido);
		
		textApellido = new JTextField();
		textApellido.setColumns(10);
		textApellido.setBounds(236, 143, 270, 20);
		contentPane.add(textApellido);
		
		lblEmail = new JLabel("Email:");
		lblEmail.setHorizontalAlignment(SwingConstants.LEFT);
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblEmail.setBounds(27, 190, 92, 38);
		contentPane.add(lblEmail);
		
		textEmail = new JTextField();
		textEmail.setColumns(10);
		textEmail.setBounds(236, 203, 270, 20);
		contentPane.add(textEmail);
		
		lblGenero = new JLabel("Genero:");
		lblGenero.setHorizontalAlignment(SwingConstants.LEFT);
		lblGenero.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblGenero.setBounds(27, 244, 92, 38);
		contentPane.add(lblGenero);
		
		textGenero = new JTextField();
		textGenero.setColumns(10);
		textGenero.setBounds(236, 257, 270, 20);
		contentPane.add(textGenero);
		
		lblContraseña = new JLabel("Contrase\u00F1a:");
		lblContraseña.setHorizontalAlignment(SwingConstants.LEFT);
		lblContraseña.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblContraseña.setBounds(27, 304, 92, 38);
		contentPane.add(lblContraseña);
		
		textContraseña = new JTextField();
		textContraseña.setColumns(10);
		textContraseña.setBounds(236, 317, 270, 20);
		contentPane.add(textContraseña);
		
		lblFechaDeNacimiento = new JLabel("Fecha de nacimiento:");
		lblFechaDeNacimiento.setHorizontalAlignment(SwingConstants.LEFT);
		lblFechaDeNacimiento.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblFechaDeNacimiento.setBounds(27, 362, 158, 38);
		contentPane.add(lblFechaDeNacimiento);
		
		textFechaDeNacimiento = new JTextField();
		textFechaDeNacimiento.setColumns(10);
		textFechaDeNacimiento.setBounds(236, 375, 270, 20);
		contentPane.add(textFechaDeNacimiento);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(this.getControlador());
		btnGuardar.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnGuardar.setBounds(236, 437, 176, 23);
		contentPane.add(btnGuardar);
	}


	public JTextField getTextNombre() {
		return textNombre;
	}


	public JLabel getLblApellido() {
		return lblApellido;
	}


	public JTextField getTextApellido() {
		return textApellido;
	}


	public JLabel getLblEmail() {
		return lblEmail;
	}


	public JTextField getTextEmail() {
		return textEmail;
	}


	public JLabel getLblGenero() {
		return lblGenero;
	}


	public JTextField getTextGenero() {
		return textGenero;
	}


	public JLabel getLblContraseña() {
		return lblContraseña;
	}


	public JTextField getTextContraseña() {
		return textContraseña;
	}


	public JLabel getLblFechaDeNacimiento() {
		return lblFechaDeNacimiento;
	}


	public JTextField getTextFechaDeNacimiento() {
		return textFechaDeNacimiento;
	}


	public ControladorPunto5 getControlador() {
		return controlador;
	}
	public void setControlador(ControladorPunto5 controlador) {
		this.controlador = controlador;
	}

	public JButton getBtnGuardar() {
		return btnGuardar;
	}



}
