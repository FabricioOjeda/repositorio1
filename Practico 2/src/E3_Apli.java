import java.util.Scanner;

public class E3_Apli {

	public static void main(String[] args) {
		E3_Persona unapersona = new E3_Persona();
		E3_Videojuego pacman = new E3_Videojuego("pacman","1980","Namco",E3_Genero.ARCADE);
		E3_Videojuego doom = new E3_Videojuego("Doom","1993","Id Software/Nerve Software",E3_Genero.SHOOTER);
		E3_Videojuego donkey = new E3_Videojuego("Donkey Kong","1981","Nintendo",E3_Genero.ARCADE);
		Scanner input = new Scanner(System.in);
		unapersona.agregarJuego(pacman);
		unapersona.agregarJuego(doom);
		unapersona.agregarJuego(donkey);
		unapersona.listarJuegos();
		System.out.println("Que juego desea quitar?");
		String eliminar = input.nextLine();
		if (unapersona.quitarJuego(eliminar ) ) {
			System.out.println("Se elimino el juego "+eliminar);
		}else {
			System.out.println("No se encuentra el juego "+eliminar);
		}
		unapersona.listarJuegos();
		input.close();
	}

}
