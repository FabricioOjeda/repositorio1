import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class E3_Persona {
private String nombre,apellido,mail,contrasena;
private Character genero;
private LocalDate fechaNacimiento;
private ArrayList<E3_Videojuego> coleccion;

public E3_Persona() {
this("","",'a', LocalDate.of(1, 1, 1),"","",new ArrayList<E3_Videojuego>());
}

public E3_Persona(String nombre, String apellido,Character genero, LocalDate fechaNacimiento,String mail, String contrasena, ArrayList <E3_Videojuego> coleccion) {
	this.setNombre(nombre);
	this.setApellido(apellido);
	this.setGenero(genero);
	this.setFechaNacimiento(fechaNacimiento);
	this.setMail(mail);
	this.setContrasena(contrasena);
	this.setColeccion(coleccion);
}

public String getNombre() {
	return nombre;
}

public void setNombre(String nombre) {
	this.nombre = nombre;
}

public String getApellido() {
	return apellido;
}

public void setApellido(String apellido) {
	this.apellido = apellido;
}
public Character getGenero() {
	return genero;
}
public void setGenero(char c) {
	this.genero = c;
}
public String getMail() {
	return mail;
}
public void setMail(String mail) {
	this.mail = mail;
}
public LocalDate getFechaNacimiento() {
	return fechaNacimiento;
}
public void setFechaNacimiento(LocalDate fechaNacimiento) {
	this.fechaNacimiento = fechaNacimiento;
}
public String getContrasena() {
	return contrasena;
}
public void setContrasena(String contrasena) {
	this.contrasena = contrasena;
}
public ArrayList<E3_Videojuego> getColeccion() {
	return coleccion;
}

public void setColeccion(ArrayList<E3_Videojuego> coleccion) {
	this.coleccion = coleccion;
}
public void agregarJuego (E3_Videojuego unJuego) {
	this.getColeccion().add(unJuego);
}
public boolean quitarJuego (String nombre) {
	//E3_Videojuego juego = new E3_Videojuego();
	//juego.setNombre(nombre);
	for (E3_Videojuego e3_Videojuego : coleccion) { //cambiar por un iterador y usar while
		if (e3_Videojuego.getNombre().equals(nombre)) {
			this.getColeccion().remove(e3_Videojuego);
			return true;
		}
	}
	return false;
}
public void listarJuegos () {
	for (E3_Videojuego e3_Videojuego : this.getColeccion()) {
		System.out.println(e3_Videojuego.infoJuego());
	}
}
}
