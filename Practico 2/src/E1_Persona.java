import java.time.LocalDate;
import javax.swing.JOptionPane;

public class E1_Persona {
//A
	private String nombre, mail,contrasena;
	private Character genero;
	private LocalDate fechaNacimiento;
//C
public E1_Persona() {
		this("",'a',"", LocalDate.of(1, 1, 1),"");
	}
	public E1_Persona(String nombre, Character genero, String mail, LocalDate fechaNacimiento, String contrasena) {
	this.setNombre(nombre);
	this.setGenero(genero);
	this.setMail(mail);
	this.setFechaNacimiento(fechaNacimiento);
	this.setContrasena(contrasena);
}

	//M
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Character getGenero() {
		return genero;
	}
	public void setGenero(char c) {
		this.genero = c;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public void imprimirInfo (E1_Persona unaPersona) {
		System.out.println("Nombre: "+ this.getNombre());
		System.out.println("Fecha de nacimiento: "+ this.getFechaNacimiento());
		System.out.println("Genero: "+ this.getGenero());
		System.out.println("Email: "+this.getMail());
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	public void ventanaContrasena () {
		String password = (JOptionPane.showInputDialog("Cual es la contraseņa de "+this.getNombre()+"?"));
		if (password.equals(this.getContrasena())) {
			String mensaje = String.format("Correcto! "+password+" es la contraseņa de "+this.getNombre());
			JOptionPane.showMessageDialog(null, mensaje);
		} else {
			String mensaje = String.format("Incorrecto! esa no es la contraseņa de"+this.getNombre());
			JOptionPane.showMessageDialog(null, mensaje);
		}
	}
}
