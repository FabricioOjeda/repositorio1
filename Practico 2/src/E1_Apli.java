import java.time.LocalDate;
import java.util.Scanner;

public class E1_Apli {

	public static void main(String[] args) {
		E1_Persona persona = new E1_Persona();
		Scanner input = new Scanner(System.in);
		System.out.println("Nombre: ");
		persona.setNombre(input.nextLine());
		System.out.println("Fecha de nacimiento");
		persona.setFechaNacimiento(LocalDate.of(input.nextInt(), input.nextInt(), input.nextInt()));
		System.out.println("Genero: ");
		persona.setGenero(input.next().charAt(0));
		System.out.println("Email: ");
		persona.setMail(input.nextLine());
		System.out.println("Contraseņa: ");
		persona.setContrasena(input.nextLine());
		System.out.println("--------------------------------------");
		persona.imprimirInfo(persona);
		persona.ventanaContrasena();
		input.close();
	}

}
