package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Scanner;

import modelo.Banco;
import modelo.Persona;
import vista.VistaPunto5;

public class ControladorPunto5 implements ActionListener{
private VistaPunto5 vista;
private Banco banco;

public ControladorPunto5() {

	this.vista = new VistaPunto5(this);
	this.banco = new Banco();
	this.vista.setVisible(true);
}
public void actionPerformed (ActionEvent e) { 
	if(e.getSource().equals(getVista().getBtnGuardar())) {
		//crear metodo en la clase banco para abm de archivo csv
		ArrayList<Persona> clientes = new ArrayList<Persona>();
		//bajo archivo csv
		getBanco().leerArchivo(clientes);
		//Guardo datos ingresados en pantalla
		String nombre = getVista().getTextNombre().getText();
		String apellido = getVista().getTextApellido().getText();
		String genero = getVista().getTextGenero().getText();
		String mail = getVista().getTextEmail().getText();
		String contrasena = getVista().getTextContraseña().getText();
		String nacimiento = getVista().getTextFechaDeNacimiento().getText();
		getBanco().agregarCuenta(new Persona(nombre+" "+apellido,genero.charAt(0),mail,nacimiento,contrasena),0.0);
		clientes.add(new Persona(nombre+" "+apellido,genero.toUpperCase().charAt(0),mail,nacimiento,contrasena));
		//datos.add(cliente);
		getBanco().listarClientes();
		//Guardo datos en el archivo csv
		getBanco().cargarArchivo(clientes);
	}
}

public VistaPunto5 getVista() {
	return vista;
}
public void setVista(VistaPunto5 vista) {
	this.vista = vista;
}
public Banco getBanco() {
	return banco;
}
public void setBanco(Banco banco) {
	this.banco = banco;
}


}
