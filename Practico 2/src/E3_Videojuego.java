
public class E3_Videojuego {
private String nombre,ano,desarrollador;
private E3_Genero generos;

public E3_Videojuego() {
	this("","","",E3_Genero.DESCONOCIDO);
}
public E3_Videojuego(String nombre, String ano, String desarrollador, E3_Genero generos) {
	this.setNombre(nombre);
	this.setAno(ano);
	this.setDesarrollador(desarrollador);
	this.setGeneros(generos);
}

public String getNombre() {
	return nombre;
}
public void setNombre(String nombre) {
	this.nombre = nombre;
}
public String getAno() {
	return ano;
}
public void setAno(String ano) {
	this.ano = ano;
}
public String getDesarrollador() {
	return desarrollador;
}
public void setDesarrollador(String desarrollador) {
	this.desarrollador = desarrollador;
}
public String infoJuego () {
	return "Nombre: " + this.getNombre() + " A�o: " + this.getAno() + " Desarrollador: " + this.getDesarrollador() + " Genero: " + this.getGeneros();
}
public E3_Genero getGeneros() {
	return generos;
}
public void setGeneros(E3_Genero generos) {
	this.generos = generos;
}
}
