import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.text.html.HTMLDocument.Iterator;

public class E4_Banco {
//A
private String nombre; //nombre del banco
private HashMap<E1_Persona, Double> cuentas = new HashMap<E1_Persona,Double>();

//C
public E4_Banco() {
this("",new HashMap<E1_Persona,Double>());
}

public E4_Banco(String nombre, HashMap<E1_Persona, Double> cuentas) {
	this.setNombre(nombre);
	this.setCuentas(cuentas);
}

//M
public String getNombre() {
	return nombre;
}
public void setNombre(String nombre) {
	this.nombre = nombre;
}
public HashMap<E1_Persona, Double> getCuentas() {
	return cuentas;
}
public void setCuentas(HashMap<E1_Persona, Double> cuentas) {
	this.cuentas = cuentas;
}
//Metodo para agregar una cuenta
public Boolean agregarCuenta(E1_Persona persona, Double saldo) {
	this.setNombre(persona.getNombre());
	cuentas.put(persona, saldo);
	return true;
}
//Metodo para eliminar una cuenta
public Boolean eliminarCuenta(E1_Persona persona) {
	if (this.getCuentas().containsKey(persona)) {
		this.getCuentas().remove(persona);
		return true;
	}
	return false;
}

//Metodo para imprimir clientes del banco
public void listarClientes () {
	java.util.Iterator<Entry<E1_Persona, Double>> it = this.getCuentas().entrySet().iterator();
	while (it.hasNext()) {
		Map.Entry<E1_Persona, Double> entry = (Entry<E1_Persona, Double>)it.next();
		E1_Persona una = new E1_Persona();
		una = entry.getKey();
		System.out.println("Nombre: "+una.getNombre()+ " saldo: "+entry.getValue());
	} 
}
public Double getSaldo (E1_Persona persona) {
	java.util.Iterator<Entry<E1_Persona, Double>> it = this.getCuentas().entrySet().iterator();
	while (it.hasNext()) {
		Map.Entry<E1_Persona, Double> entry = (Entry<E1_Persona, Double>)it.next();
		E1_Persona una = new E1_Persona();
		una = entry.getKey();
		if (una.getNombre().equals(persona.getNombre())) {
			return entry.getValue();
		}
	}
	return 0.0; 
	}
}


