import java.time.LocalDate;

public class Trabajador extends Empleado{
private Double sueldo,retenciones,impuestos,premios;
	public Trabajador(Integer nroLegajo, Integer telefono, String nombre, String puesto, String direccion,
			LocalDate fechaNacimiento, LocalDate fechaContratacion, Double sueldo, Double retenciones, Double impuestos, Double premios) {
		super(nroLegajo, telefono, nombre, puesto, direccion, fechaNacimiento, fechaContratacion);
		this.sueldo = sueldo;
		this.retenciones = retenciones;
		this.impuestos = impuestos;
		this.premios = premios;
	}
	public Double getSueldo() {
		return sueldo;
	}
	public Double getRetenciones() {
		return retenciones;
	}
	public Double getImpuestos() {
		return impuestos;
	}
	public Double getPremios() {
		return premios;
	}
	@Override
	public Double calcularSueldo() {
		return this.getSueldo()-this.getRetenciones()-this.getImpuestos()+this.getPremios();
	}
	public String toString() {
		return super.toString()+"\nSueldo: "+this.calcularSueldo()+"\nRetenciones: "+this.getRetenciones()+"\nImpuestos: "+this.getImpuestos()+"\nPremios: "+this.getPremios();
	}
}
