import java.time.LocalDate;

public abstract class Empleado {
private Integer nroLegajo,telefono;
private String nombre,puesto,direccion;
private LocalDate fechaNacimiento, fechaContratacion;


public Empleado(Integer nroLegajo, Integer telefono, String nombre, String puesto, String direccion,
		LocalDate fechaNacimiento, LocalDate fechaContratacion) {
	this.nroLegajo = nroLegajo;
	this.telefono = telefono;
	this.nombre = nombre;
	this.puesto = puesto;
	this.direccion = direccion;
	this.fechaNacimiento = fechaNacimiento;
	this.fechaContratacion = fechaContratacion;
}
public Integer getNroLegajo() {
	return nroLegajo;
}
public Integer getTelefono() {
	return telefono;
}
public String getNombre() {
	return nombre;
}
public String getPuesto() {
	return puesto;
}
public String getDireccion() {
	return direccion;
}
public LocalDate getFechaNacimiento() {
	return fechaNacimiento;
}
public LocalDate getFechaContratacion() {
	return fechaContratacion;
}
@Override
public String toString() {
	return "Empleado: "+getNombre()+"\nFecha de nacimiento: "+getFechaNacimiento()+"\nNro legajo: "+getNroLegajo()+"\nPuesto: "+getPuesto()+"\nFecha de contratacion: "+getFechaContratacion()+"\nDireccion: "+getDireccion()+"\nTelefono: "+getTelefono();
}
public abstract Double calcularSueldo();
}
