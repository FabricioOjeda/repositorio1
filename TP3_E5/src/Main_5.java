import java.time.LocalDate;
import java.util.ArrayList;

public class Main_5 {
public static void main(String[] args) {
	Trabajador rodrigo = new Trabajador(313, 154982355, "Rodrigo Savoye", "Ingeniero informatico", "Pascalito 1490", LocalDate.of(1996, 11, 9), LocalDate.of(2021, 5, 21),200000.00 , 5000.0, 10000.0, 20000.0);
	Contratado fabricio = new Contratado(212, 155066416, "Fabricio Ojeda", "Analista Programador Universitario", "Cod 2437 casa 18", LocalDate.of(1996,8,24), LocalDate.of(2021, 8, 30),2000.0,50.0);
	ArrayList<Empleado> empleados = new ArrayList<Empleado>();
	empleados.add(fabricio);
	empleados.add(rodrigo);
	for (Empleado empleado : empleados) {
		System.out.println(empleado.toString()+"\n\n");
	}
}
}
