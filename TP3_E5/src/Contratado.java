import java.time.LocalDate;

public class Contratado extends Empleado {
private Double tarifaHora, CantidadHorasMes;
	public Contratado(Integer nroLegajo, Integer telefono, String nombre, String puesto, String direccion,
			LocalDate fechaNacimiento, LocalDate fechaContratacion, Double tarifaHora, Double cantidadHorasMes) {
		super(nroLegajo, telefono, nombre, puesto, direccion, fechaNacimiento, fechaContratacion);
		this.tarifaHora = tarifaHora;
		this.CantidadHorasMes = cantidadHorasMes;
	}
	public Double getTarifaHora() {
		return tarifaHora;
	}
	public Double getCantidadHorasMes() {
		return CantidadHorasMes;
	}
	@Override
	public Double calcularSueldo() {
		return this.getCantidadHorasMes()*this.getTarifaHora();
	}
	public String toString() {
		return super.toString()+"\nTarifa por hora: "+this.getTarifaHora()+"\nCantidad de horas al mes: "+this.getCantidadHorasMes()+"\nSueldo: "+this.calcularSueldo();
	}
}
