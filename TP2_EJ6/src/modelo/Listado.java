package modelo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Scanner;

public class Listado {
private ArrayList<Persona> personas = new ArrayList<Persona>();

public Listado() {
this(new ArrayList<Persona>());
}

public Listado(ArrayList<Persona> personas) {
	this.personas = personas;
}

public ArrayList<Persona> getPersonas() {
	return personas;
}

public void setPersonas(ArrayList<Persona> personas) {
	this.personas = personas;
}
public ArrayList<Persona> leerArchivo (ArrayList<Persona> datos){
	//bajo archivo csv
	try (Scanner input = new Scanner(Paths.get("otros/listapersonas.csv"))){
		while(input.hasNext()) {
			Persona unapersona = new Persona(input.next()+" "+input.next(),input.next().charAt(0), input.next(),LocalDate.parse(input.next()),"");
			datos.add(unapersona);
		}
	} catch (IOException a) {
		a.printStackTrace();
	}
	return datos;
}
public void cargarArchivo (ArrayList<Persona> datos) {
	try (Formatter output = new Formatter ("otros/listapersonas.csv")){
		for (Persona persona : datos) {
			output.format("%s %c %s %s %s%n",persona.getNombre(),persona.getGenero(),persona.getMail(),persona.getFechaNacimiento(),persona.getContrasena());
		}
	}  catch (FileNotFoundException exc) {
		exc.printStackTrace();
	}
}
}
