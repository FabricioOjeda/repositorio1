package controlador;

import java.awt.EventQueue;
import java.time.LocalDate;
import java.util.ArrayList;

import javax.swing.table.DefaultTableModel;

import modelo.Listado;
import modelo.Persona;
import vista.Vista6;

public class ControladorE6 {
	private Vista6 vista;
	private Listado listado;

	public ControladorE6() {
		this.vista = new Vista6(this);
		this.listado = new Listado();
		this.vista.setVisible(true);
		llenarFilas();
	}
	public Vista6 getVista() {
		return vista;
	}
	public void setVista(Vista6 vista) {
		this.vista = vista;
	}
	public Listado getListado() {
		return listado;
	}
	public void setListado(Listado listado) {
		this.listado = listado;
	}
	public void llenarFilas() { //modificar datos de la fila
		ArrayList<Persona> personas = new ArrayList<Persona>();
		getListado().leerArchivo(personas);
		for (Persona unapersona : personas) {			
			String nombre = unapersona.getNombre();
			Character genero = unapersona.getGenero();
			String email = unapersona.getMail();
			LocalDate fecha = unapersona.getFechaNacimiento();
			this.getVista().cargarFila(nombre, genero.toString(), email, fecha.toString());
		}
		getListado().cargarArchivo(personas);
	}
}
